#!/bin/bash
sudo add-apt-repository ppa:devacom/x11 -y
sudo add-apt-repository -y ppa:devacom/gnome-40
sudo add-apt-repository ppa:devacom/gnome-41 -y 
sudo apt install sssd autofs autodir autofs-ldap -y
cat example > /etc/sssd/conf.d/sssd.conf
cp Google_2025_02_15_26337.* /var/.
sudo echo "session    required    pam_mkhomedir.so skel=/etc/skel/ umask=0022" >> /etc/pam.d/common-session
sudo chown root:root /etc/sssd/conf.d/sssd.conf && sudo chmod 600 /etc/sssd/conf.d/sssd.conf
sudo echo "%itsupportgroup ALL=(ALL:ALL) ALL" >> /etc/sudoers
sudo rm -rf /etc/apt/sources.list.d/devacom-ubuntu* && sudo add-apt-repository -r -y ppa:devacom/x11
sudo add-apt-repository -r -y ppa:devacom/gnome-40 && sudo add-apt-repository -r -y ppa:devacom/gnome-41
sudo systemctl restart autofs.service autodir.service sssd.service
echo Installed Successfully Please reboot your machine
sudo rm -rf /etc/apt/sources.list.d/devacom-ubuntu*
